//
//  ContentView.swift
//  SqliteVideo_Practical
//
//  Created by Rajvi on 31/08/21.
//

import SwiftUI

struct ContentView: View {
    
    @State var userModels : [UserModel] = []
    @State var userSelected: Bool = false
    @State var selectedUserId: Int64 = 0
    var body: some View {
        ZStack{
           
        NavigationView{
            
            VStack{
                HStack{
                    NavigationLink(
                        destination: AddUserView(),
                        label: {
                            Text("Add user")
                        })
                    NavigationLink (destination: EditUserView(id: self.$selectedUserId), isActive: self.$userSelected) {
                        EmptyView()
                    }
                }
                
                List (self.userModels) { (model) in
                    HStack {
                        Text(model.name)
                        Spacer()
                        Text(model.email).font(.system(size: 12))
                        Spacer()
                        Text("\(model.age)")
                        Spacer()
                        Button(action: {
                            self.selectedUserId = model.id
                            self.userSelected = true
                        }, label: {
                            Image("edit").resizable().frame(height: 20).scaledToFit() .frame(width: 20)
                        })
                        .buttonStyle(PlainButtonStyle())
                        Button(action: {
                            let dbManager: DB_Manager = DB_Manager()
                            dbManager.deleteUser(idValue: model.id)
                            self.userModels = dbManager.getUsers()
                        }, label: {
                            Image("delete_red").resizable().frame(height: 20).scaledToFit() .frame(width: 20)
                        })
                        .buttonStyle(PlainButtonStyle())
                    }
                }
                
            }
            .onAppear(perform: {
                self.userModels = DB_Manager().getUsers()
            })
                .navigationBarTitle(Text("Users").font(.subheadline), displayMode: .large)
           
        }
    }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
