//
//  EditUserView.swift
//  SqliteVideo_Practical
//
//  Created by Rajvi on 01/09/21.
//

import SwiftUI
struct EditUserView: View {
    @Binding var id: Int64
    @State var name: String = ""
    @State var email: String = ""
    @State var age: String = ""

    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var body: some View {
        VStack {
            
            TextField("Enter name", text: $name) .multilineTextAlignment(TextAlignment.center)
                .padding(10)
                .background(Color(.systemGray6))
                .cornerRadius(20)
                .disableAutocorrection(true)
            
            TextField("Enter email", text: $email)
                .multilineTextAlignment(TextAlignment.center)
                .padding(10)
                .background(Color(.systemGray6))
                .cornerRadius(20)
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                .disableAutocorrection(true)
            
            TextField("Enter age", text: $age)
                .multilineTextAlignment(TextAlignment.center)
                .padding(10)
                .background(Color(.systemGray6))
                .cornerRadius(20)
                .keyboardType(.numberPad)
                .disableAutocorrection(true)
            
            Button(action: {
                DB_Manager().updateUser(idValue: self.id, nameValue: self.name, emailValue: self.email, ageValue: Int64(self.age) ?? 0)
                
                self.mode.wrappedValue.dismiss()
            }, label: {
                Text("Edit User")
            })
            .frame(maxWidth: .infinity)
        }
        
        
        .onAppear(perform: {
            let userModel: UserModel = DB_Manager().getUsers(idValue: self.id)
            self.name = userModel.name
            self.email = userModel.email
            self.age = String(userModel.age)
        })
    }
}

struct EditUserView_Previews: PreviewProvider {
    
    @State static var id: Int64 = 0
    
    static var previews: some View {
        EditUserView(id: $id)
    }
}
