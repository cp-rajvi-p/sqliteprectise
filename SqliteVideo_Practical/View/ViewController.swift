//
//  ViewController.swift
//  SqliteVideo_Practical
//
//  Created by Rajvi on 01/09/21.
//

import UIKit
import SwiftUI

class ViewController: UIViewController {
    
    let contentView = UIHostingController(rootView: ContentView())
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(contentView.view)
    }
}
